# [EDIT - Maj 21/02/2024]
- added git defautl behaviour
- multi platform support
- merge...

# [EDIT - Maj 16/01/2024]

Then create a git on gitlab : new project --> blank project --> warning incoming 
warning : unstick the "initialize with a readme" 

copy from git / local folder the repo py-boilerplate-base 
change the repo name 
run service-init.sh to create the .env 

open .env 
change docker namespace / docker_name if necessarily 
and other parameters 
edit git config inside .env 

run ./service-git-init.sh

commit 
publish branch 

ok etape II  



# Pokapok Boilerplate - python mamba Docker 

This repo is a boilerplate for python development based on mamba combined with Poetry. 

This README explain the behaviour of the container 

It will use the script so-start-project.sh to creates the entire execution capsule. 

example :

First, as a good practice, copy this repo in a pok-boilerplate repo under your home like : 
/nfs-home/pokapok/username/pok-boilerplates/py-boilerplate-base 
git clone ...

Then switch to mamba branch
git checkout mambaPoetry

Then, make the script so-start-project.sh executable from anywhere to create a project whenever you want
copy this line into your .bashrc file (in /nfs-home/pokapok/username/.bashrc):
    export PATH="$PATH:~/pok-boilerplates/pok-python-boilerplates/py-boilerplate-base"  
and run:
    chmod +x ~/pok-boilerplates/pok-python-boilerplates/py-boilerplate-base/* to give permissions 

Then launch the script and answer the questions 
example of launching the script : 
    so-start-project.sh --install-opt POETRY --nameproject TESTPROJ --location /nfs-home/pokapok/cweber/my-projects/. --pyversion 3.10

To launch Shiny after build : 
    - edit the .env and put the START_OPTION=SHINY
    - Inside the docker, launch : ./launchers/start-app.sh 
