#!/bin/bash

# human readable time
START_TIME=$(date --utc +%Y%m%d%_H%M%S)
USER_ID=${FORCE_USER_ID:-1000}
GROUP_ID=${FORCE_GROUP_ID:-1000}


# runtime log
START_LOGDIR=/home/serviceuser/run
START_LOG=$START_LOGDIR/pok_appstart.log

# Add local user
# Either use the LOCAL_USER_ID if passed in at runtime or
# fallback
echo "---------------------------------- $(whoami)" 
echo "--- $START_TIME" 
echo "----------------------------------" 
echo "--- " 
echo "Starting with UID : $USER_ID - GID $GROUP_ID" 

# useradd --shell /bin/bash -u $USER_ID -o -c "" -m user
# export HOME=/home/user

if [ $GROUP_ID != 1000 ];
then
  # rm /etc/.pwd.lock
  groupmod -g ${GROUP_ID} -o servicegroup
  echo "  GID changed : $GROUP_ID" 
else
  echo "  GID default : 1000" 
fi

if [ $USER_ID != 1000 ];
then
  usermod -u ${USER_ID} -o serviceuser
  echo "  UID changed : $USER_ID" 
else
  echo "  UID default : 1000" 

fi

# change main directories owners / permissions
# chown -R serviceuser:servicegroup /home/serviceuser
# echo "directories owner updated : " 
# echo " /home/serviceuser" 

echo "passing command : [$@]" 

exec /usr/sbin/gosu serviceuser "$@"