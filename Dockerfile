#                        ██████╗  █████╗ ███████╗███████╗                    
#                        ██╔══██╗██╔══██╗██╔════╝██╔════╝                    
#    █████╗    █████╗    ██████╔╝███████║███████╗█████╗      █████╗    █████╗
#    ╚════╝    ╚════╝    ██╔══██╗██╔══██║╚════██║██╔══╝      ╚════╝    ╚════╝
#                        ██████╔╝██║  ██║███████║███████╗                    
#                        ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝                    


FROM ubuntu:22.04 as python-base


# ---- ARGS & ENV ----
ARG PYTHON_VERSION
ARG FORCE_USER_ID
ARG FORCE_GROUP_ID
ARG FORCE_USER_PWD

ARG PYTHON_VERSION
# ARG POETRY_VERSION

ENV FORCE_USER_ID=${FORCE_USER_ID}
ENV FORCE_GROUP_ID=${FORCE_GROUP_ID}

ENV PYTHON_VERSION=${PYTHON_VERSION}
ENV PYTHONNOUSERSITE=True
# ENV POETRY_VERSION=${POETRY_VERSION}

ENV DEBIAN_FRONTEND=noninteractive
ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
ENV POETRY_VIRTUALENVS_CREATE=false



# ------------------ ROOT USER ------------------
USER root

# ---- INSTALL UTILS & PYTHON ----
RUN apt-get update \
    && apt-get --yes install apt-utils \
    && apt-get --yes install curl wget \
    && apt-get --yes install openssh-client \
    && apt-get --yes install sudo \
    && apt-get --yes install gosu \
    && apt-get --yes install net-tools \
    && apt-get --yes install iputils-ping

RUN apt-get --yes upgrade
RUN apt-get --yes install git

# ---- SET UID/GID ----
# add a user with same GID and UID as the host user that owns the workspace files on the host (bind mount)
RUN groupadd -f servicegroup -g ${FORCE_GROUP_ID}
RUN useradd -s $(which bash) --uid ${FORCE_USER_ID} --gid ${FORCE_GROUP_ID} -m serviceuser
RUN echo serviceuser:${FORCE_USER_PWD}
RUN echo "serviceuser:${FORCE_USER_PWD}" | chpasswd
RUN usermod -aG sudo serviceuser
RUN echo "serviceuser ALL=(ALL) NOPASSWD:SETENV: /usr/local/bin/entrypoint.sh" >> /etc/sudoers

# ---- GIVE PERMISSIONS ----
RUN chown -R serviceuser:servicegroup /home/serviceuser
# RUN ls -la /home/serviceuser

# ---- COPY & RIGHTS ENTRYPOINT ----
COPY ./docker-entrypoint/entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

# ---- SET ENTRYPOINT ---- 
ENTRYPOINT ["sudo", "-E", "/usr/local/bin/entrypoint.sh"]



#                 ███╗   ███╗ █████╗ ███╗   ███╗██████╗  █████╗     ███████╗████████╗ █████╗  ██████╗ ███████╗                
#                 ████╗ ████║██╔══██╗████╗ ████║██╔══██╗██╔══██╗    ██╔════╝╚══██╔══╝██╔══██╗██╔════╝ ██╔════╝                
# █████╗█████╗    ██╔████╔██║███████║██╔████╔██║██████╔╝███████║    ███████╗   ██║   ███████║██║  ███╗█████╗      █████╗█████╗
# ╚════╝╚════╝    ██║╚██╔╝██║██╔══██║██║╚██╔╝██║██╔══██╗██╔══██║    ╚════██║   ██║   ██╔══██║██║   ██║██╔══╝      ╚════╝╚════╝
#                 ██║ ╚═╝ ██║██║  ██║██║ ╚═╝ ██║██████╔╝██║  ██║    ███████║   ██║   ██║  ██║╚██████╔╝███████╗                
#                 ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝╚═════╝ ╚═╝  ╚═╝    ╚══════╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚══════╝                
                                                                                                                            
FROM python-base as mamba_stage


# ---- ARGS & ENV ----
ARG GIT_USER_NAME
ARG GIT_USER_EMAIL

ENV GIT_USER_NAME=${GIT_USER_NAME}
ENV GIT_USER_EMAIL=${GIT_USER_EMAIL}



# ------------------ ROOT USER ------------------
WORKDIR /home/serviceuser

RUN mkdir -p /home/serviceuser/service/app
RUN mkdir -p /home/serviceuser/log
RUN mkdir -p /home/serviceuser/run
RUN mkdir -p /home/serviceuser/.ssh

COPY README.md ./service/.
RUN ls -la /home/serviceuser/service


WORKDIR /home/serviceuser/service/app

# ---- COPY APP requirements
COPY app ./

# ---- SSH EVAL configuration
RUN mkdir -p /home/serviceuser/.ssh
RUN chmod 700 /home/serviceuser/.ssh
COPY resources/SSH/config /home/serviceuser/.ssh/config
COPY secrets-ssh/git-* /home/serviceuser/.ssh
RUN ls -la  /home/serviceuser/.ssh/
RUN chmod -R 600 /home/serviceuser/.ssh/*
RUN ssh-keyscan gitlab.com >> /home/serviceuser/.ssh/known_hosts
RUN eval "$(ssh-agent -s)" && \
    ssh-add /home/serviceuser/.ssh/git-ssh-key

# ---- ensure service user ownership
RUN chown -R serviceuser:servicegroup /home/serviceuser


# ------------------ SERVICEUSER USER ------------------
USER serviceuser

# - - - - INSTALL MINIFORGE3
RUN curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh" 
RUN chmod +x Miniforge3-$(uname)-$(uname -m).sh
RUN ls -la
RUN bash Miniforge3-$(uname)-$(uname -m).sh -b -p /home/serviceuser/miniforge3

# - - - - INSTALL MAMBA ENV
RUN /home/serviceuser/miniforge3/bin/mamba env create -n env-develop -f environment.yml

#       Default shell for next mamba envs installs
SHELL ["/home/serviceuser/miniforge3/bin/mamba", "run", "-n", "env-develop", "/bin/bash", "-c"]

# - - - -  Insert BASH content
RUN /home/serviceuser/miniforge3/bin/mamba init
RUN echo "PATH=${PATH}:/home/serviceuser/bin:/home/serviceuser/service/app/launchers"  >> ~/.bashrc
RUN echo "cd /home/serviceuser/service/app"  >> ~/.bashrc
RUN echo "mamba activate env-develop" >> ~/.bashrc

# - - - -  Git settings
RUN git config --global user.name "${GIT_USER_NAME}"
RUN git config --global user.email "${GIT_USER_EMAIL}"
# RUN git config --global pull.rebase true
RUN git config --global pull.ff only
RUN git config --global merge.ff false


# - - - - clean user secrets so that it is not pushed with the container image
RUN rm /home/serviceuser/.ssh/git-*

# - - - - command for the startup
CMD ["bash", "-c", "/home/serviceuser/service/app/launchers/start-app.sh"]
