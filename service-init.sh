#!/bin/bash
echo =====================================
echo service initialization
echo -------------------------------------


# - - functions

# ---------------------------------------------------------------------------------------------------

# - - - current user informations
MY_UID=$(id -u)
MY_GID=$(id -g)
POK_DEFAULT_SSH_KEY="$HOME/.ssh/pok-ed_$USER"

# - - - create .env file if not exists
echo "*** creating .env if not exists - - - -"

if [ -f ".env" ]
then
    echo "*** .env already exists"
else
    echo "*** new .env from .env.example"
    cp .env.example .env

    # -- POKAPOK USER ENV DEFAULTS
    # --- SSH KEY
    if [ -f "$POK_DEFAULT_SSH_KEY" ]
    then
        echo "**** found Pokapok standard user ssh key : $POK_DEFAULT_SSH_KEY"
        case $(uname) in
            Darwin)
                echo "INFO: Arch = MacOS"
                sed -i '' -e "s|SSH_KEY=.*|SSH_KEY=$POK_DEFAULT_SSH_KEY|g" .env
                ;;
            Linux)
                echo "INFO: Arch = Linux"
                sed -i "s|SSH_KEY=.*|SSH_KEY=$POK_DEFAULT_SSH_KEY|g" .env
                ;;
            *)
                echo "ERROR: Arch is not defined! Are you using Linux or MacOS? Exiting!"
                exit 1
                ;;         
        esac
    else
        echo "**** no default public key found."
    fi

    # --- USER NAME
    if [[ -z "${POK_MY_FULL_NAME}" ]]
    then
        echo "**** no default user name found."
    else
        echo "**** found Pokapok standard user name : $POK_MY_FULL_NAME"
        case $(uname) in
            Darwin)
                echo "INFO: Arch = MacOS"
                sed -i '' -e "s|GIT_USER_NAME=.*|GIT_USER_NAME=$POK_MY_FULL_NAME|g" .env
                ;;
            Linux)
                echo "INFO: Arch = Linux"
                sed -i "s|GIT_USER_NAME=.*|GIT_USER_NAME=$POK_MY_FULL_NAME|g" .env
                ;;
            *)
                echo "ERROR: Arch is not defined! Are you using Linux or MacOS? Exiting!"
                exit 1
                ;;         
        esac
    fi

    # --- USER EMAIL
    if [[ -z "${POK_MY_EMAIL}" ]]
    then
        echo "**** no default user email found."
    else
        echo "**** found Pokapok standard user email : $POK_MY_EMAIL"
        case $(uname) in
            Darwin)
                echo "INFO: Arch = MacOS"
                sed -i '' -e "s|GIT_USER_EMAIL=.*|GIT_USER_EMAIL=$POK_MY_EMAIL|g" .env
                ;;
            Linux)
                echo "INFO: Arch = Linux"
                sed -i "s|GIT_USER_EMAIL=.*|GIT_USER_EMAIL=$POK_MY_EMAIL|g" .env
                ;;
            *)
                echo "ERROR: Arch is not defined! Are you using Linux or MacOS? Exiting!"
                exit 1
                ;;         
        esac
    fi


fi


# - - - PLATFORM & LOCAL USER
case $(uname) in
    Darwin)
        echo "INFO: Arch = MacOS"
        PLATFORM=linux/arm64/v8
        sed -i '' -e "s|PLATFORM=.*|PLATFORM=$PLATFORM|g" .env
        sed -i '' -e "s|FORCE_USER_ID=.*|FORCE_USER_ID=$MY_UID|g" .env
        sed -i '' -e "s|FORCE_GROUP_ID=.*|FORCE_GROUP_ID=$MY_GID|g" .env
        ;;
    Linux)
        echo "INFO: Arch = Linux"
        PLATFORM=linux/amd64
        sed -i "s|PLATFORM=.*|PLATFORM=$PLATFORM|g" .env
        sed -i "s|FORCE_USER_ID=.*|FORCE_USER_ID=$MY_UID|g" .env
        sed -i "s|FORCE_GROUP_ID=.*|FORCE_GROUP_ID=$MY_GID|g" .env
        ;;
    *)
      echo "ERROR: Arch is not defined! Are you using Linux or MacOS? Exiting!"
      exit 1
      ;;
esac


# - - - Create SSH secrets folder and copy developer key inside
echo "*** creating secrets folder - - - -"

# - - - get the SSH_KEY from env and expands to full path
SSH_KEY=$(eval echo $(cat .env | grep SSH_KEY | awk -F"=" '{print $2}') )

# - - - copy key in a local folder for developper volume mount
mkdir -p ./secrets-ssh
cp ./resources/SSH/config ./secrets-ssh

# - - - - copy private key
if  [ -f "${SSH_KEY}" ]
then
    cp ${SSH_KEY} ./secrets-ssh/git-ssh-key
else
    echo "**** private key not found,  it should have been : ${SSH_KEY}"
    echo "****    -> fill the .env/SSH_KEY information and execute ./service-init.sh again"
fi

# - - - - copy public key
if  [ -f "${SSH_KEY}.pub" ]
then
    cp ${SSH_KEY}.pub ./secrets-ssh/git-ssh-key.pub
else
    echo "**** public key not found , it should have been : ${SSH_KEY}.pub"
fi

# - - - - set correct folder permissions
chmod 700 ./secrets-ssh
chmod 600 ./secrets-ssh/*

# - - end of task
echo
echo " service init done."
echo "====================================="
echo
