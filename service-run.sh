#!/bin/bash

PACKAGE_NAME=`cat .env | grep PACKAGE_NAME | awk -F"=" '{print $2}'`

docker-compose exec  -u serviceuser -w /home/serviceuser/service/${PACKAGE_NAME}/${PACKAGE_NAME} pok_app python main-${PACKAGE_NAME}.py