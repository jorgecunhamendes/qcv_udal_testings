# here is the API Version that should be tagged on your project
# -------------------------------------------------------------
#
# [YEAR]-[MONTH]-[D{letters for datamodel}-E{digits for environment}]
#
#  also store informations in :
#   datamodel history in DATAMODEL_HISTORY.md
#   environment options history in ENVIRONMENT_HISTORY.md
#

API_VERSION=2023-12-DA-E01