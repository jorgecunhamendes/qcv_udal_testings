
#       imports 
import yaml
import numpy as np
from sys import version
import xarray as xr
import pandas as pd
import matplotlib.pyplot 
import cmocean

#       python version
print(f"py_version = {version}")
print(f"np_version = {np.version.version}")

#       main

if __name__ == '__main__':
    print('...app general entrypoint...')
