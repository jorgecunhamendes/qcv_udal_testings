# ███████╗██╗   ██╗███╗   ██╗ ██████╗    ██████╗     ████████╗███████╗███████╗████████╗    ██╗   ██╗██████╗  █████╗ ██╗     
# ██╔════╝██║   ██║████╗  ██║██╔════╝    ╚════██╗    ╚══██╔══╝██╔════╝██╔════╝╚══██╔══╝    ██║   ██║██╔══██╗██╔══██╗██║     
# █████╗  ██║   ██║██╔██╗ ██║██║          █████╔╝       ██║   █████╗  ███████╗   ██║       ██║   ██║██║  ██║███████║██║     
# ██╔══╝  ██║   ██║██║╚██╗██║██║         ██╔═══╝        ██║   ██╔══╝  ╚════██║   ██║       ██║   ██║██║  ██║██╔══██║██║     
# ██║     ╚██████╔╝██║ ╚████║╚██████╗    ███████╗       ██║   ███████╗███████║   ██║       ╚██████╔╝██████╔╝██║  ██║███████╗
# ╚═╝      ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝    ╚══════╝       ╚═╝   ╚══════╝╚══════╝   ╚═╝        ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝                                                                                                                           

import xarray as xr





#  █████╗ ██████╗  ██████╗  ██████╗ 
# ██╔══██╗██╔══██╗██╔════╝ ██╔═══██╗
# ███████║██████╔╝██║  ███╗██║   ██║
# ██╔══██║██╔══██╗██║   ██║██║   ██║
# ██║  ██║██║  ██║╚██████╔╝╚██████╔╝
# ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝  ╚═════╝ 


def checks(file, lst_dims, attrs_keys_to_test, vars_to_test):
    """ open and test if dimensions written in lst_dims are in file
    In case of failure, name of the file is written in lst errs """
    dict_errs = {}
    ds = xr.open_dataset(file)
    for dim in lst_dims:
        if dim not in list(ds.dims):
            dict_errs[dim] = file
    
    for attr in attrs_keys_to_test:
        if attr not in list(ds.attrs.keys()):
            dict_errs[attr] = file

    for var in vars_to_test:
        if var not in list(ds.data_vars.keys()):
            dict_errs[var] = file

    return dict_errs