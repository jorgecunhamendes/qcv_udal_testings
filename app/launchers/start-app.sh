#!/bin/bash

# execution log
APP_LOGDIR=/home/serviceuser/log
APP_FRONTEND_LOG=$APP_LOGDIR/pok_front_app.log
APP_BACKEND_LOG=$APP_LOGDIR/pok_back_app.log

touch $APP_FRONTEND_LOG
touch $APP_BACKEND_LOG

echo - - - - - - - - - - - - - $(whoami)

# echo - - home directory
# ls -la ~/
# echo - -

source /home/serviceuser/.bashrc

# cat /home/serviceuser/.bashrc

echo starting option is : $START_OPTION
echo data vol is : $DATA_VOLUME
echo internal port is : $INTERNAL_PORT_FRONTEND
echo external port is : $EXPOSED_PORT_FRONTEND
echo internal port is : $INTERNAL_PORT_BACKEND
echo external port is : $EXPOSED_PORT_BACKEND

# /opt/conda/bin/conda run -n env-develop /bin/bash -c /home/serviceuser/service/app/launchers/lock-app.sh

if [ $# -gt 0 ]; then
    start_option=$1

else
    start_option=$START_OPTION
echo "Chosen start option is : $start_option"
fi

case ${start_option} in
    "SHINY")
        /home/serviceuser/miniforge3/envs/env-develop/bin/shiny run --reload --host 0.0.0.0 --port $INTERNAL_PORT /home/serviceuser/service/app/src_python/shiny_frontend/app.py >> $APP_FRONTEND_LOG
        ;;
    "SHINYDEV")
        /home/serviceuser/miniforge3/envs/env-develop/bin/shiny run --reload --host 0.0.0.0 --port $INTERNAL_PORT /home/serviceuser/service/app/src_python/shiny_frontend/app.py
        ;;
    "FASTAPI")
        cd /home/serviceuser/service/app && uvicorn src_python.fast_api.test_app:app --reload --host 0.0.0.0 --port $INTERNAL_PORT_BACKEND >> $APP_BACKEND_LOG
        ;;
    "BASE")
        python /home/serviceuser/service/app/main-app.py
        ;;
    "BASH")
        /bin/bash
        ;;
    "FULLSTACK")
        cd /home/serviceuser/service/app && uvicorn src_python.fast_api.test_app:app --reload --host 0.0.0.0 --port $INTERNAL_PORT_BACKEND >> $APP_BACKEND_LOG
        /home/serviceuser/miniforge3/envs/env-develop/bin/shiny run --reload --host 0.0.0.0 --port $INTERNAL_PORT /home/serviceuser/service/app/src_python/shiny_frontend/app.py >> $APP_FRONTEND_LOG
        ;;
    *)
        echo no option passed .....
        /bin/bash    
        ;;
esac