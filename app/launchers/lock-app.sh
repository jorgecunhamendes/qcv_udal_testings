#!/bin/bash

# conda run -n env-develop /bin/bash -c


if [ -f /home/serviceuser/service/app/poetry.lock ]; then 
    cd home/serviceuser/service/app
    echo poetry.lock already exists...
  else
    cd home/serviceuser/service/app
    echo creating new poetry.lock...
    poetry install --no-cache
  fi

echo poetry check done.